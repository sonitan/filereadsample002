package com.c0113212.filereadsample002;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements Runnable{

    //WiFi Direct対応デバイス(P2Pデバイス)の情報、接続状態を管理するクラス
    private WifiP2pManager mWifiP2pManager;
    /** Channel */
    private WifiP2pManager.Channel mChannel;
    private final IntentFilter intentFilter = new IntentFilter();
    /** リスナアダプタ */
    private ActionListenerAdapter mActionListenerAdapter;
    private BroadcastReceiver receiver = null;
    /** Wi-Fi Direct 有効/無効状態 */
    private boolean mIsWiFiDirectEnabled;
    /** BroadcastReceiver 全部 */
    private BroadcastReceiver mReceiver;
    /** BroadcastReceiver P2P_STATE_CHANGED_ACTION */
    private WDBR_P2P_STATE_CHANGED_ACTION mWDBR_P2P_STATE_CHANGED_ACTION;
    /** BroadcastReceiver P2P_PEERS_CHANGED_ACTION */
    private WDBR_P2P_PEERS_CHANGED_ACTION mWDBR_P2P_PEERS_CHANGED_ACTION;
    /** BroadcastReceiver P2P_CONNECTION_CHANGED_ACTION */
    private WDBR_P2P_CONNECTION_CHANGED_ACTION mWDBR_P2P_CONNECTION_CHANGED_ACTION;
    /** BroadcastReceiver THIS_DEVICE_CHANGED_ACTION */
    private WDBR_P2P_THIS_DEVICE_CHANGED_ACTION mWDBR_THIS_DEVICE_CHANGED_ACTION;
    String formatedIpAddress;
    //DownloadPath
    File file05 = Environment.getExternalStorageDirectory();
    //    自分のmacアドレス情報
    BluetoothAdapter mybluetoothAdapter01 =
            BluetoothAdapter.getDefaultAdapter();
    ProgressDialog progressDialog;
    //ペアリングされていない端末の情報格納(ListViewにつかう)
    ArrayList<String> btname01 = new ArrayList<String>();
    //ペアリングされてない端末の情報格納(BluetoothClientに使う)
    ArrayList<BluetoothDevice> btinfor01 = new ArrayList<BluetoothDevice>();
    Thread thread;

    /**
     * Wifi-Direct用BroadCast
     */
    /** BroadcastReceiver */
    private enum ReceiverState {
        All,
        StateChange,
        PeersChange,
        ConnectionChange,
        ThisDeviceChange,
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("みて", file05.getPath());
        //NanoHTTPD Start
        Button nanostart = (Button)findViewById(R.id.N_Start);
        nanostart.setOnClickListener(new StartButtonClickListener01());
        //WifiDirectStart
        Button wifiStart = (Button)findViewById(R.id.W_Start);
        wifiStart.setOnClickListener(new StartButtonClickListener02());
        //WifiDirectStop
        Button wifistop =(Button)findViewById(R.id.W_Stop);
        wifistop.setOnClickListener(new StopButtonClickListener02());
        //Bluetooth検索許可
        Button btFind = (Button)findViewById(R.id.B_Find);
        btFind.setOnClickListener(new BluetoothFind());
        //Bluetooth創作
        Button btSearch = (Button)findViewById(R.id.B_Pearing);
        btSearch.setOnClickListener(new BluetoothSearch());

        //WiFi Directの有効/無効状態
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        //デバイス情報の変更通知（通信可能なデバイスの発見・ロストなど）
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        //IPアドレスなどコネクション情報。通信状態の変更通知
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        //自分自身のデバイス状態の変更通知(相手デバイスではないことに注意)
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

//        WifiP2pManagerを取得し、initializeメソッドで初期化します。
//        WiFi機能を使うContext(ソースコードでは第1引数のthis)とスレッド(getMainLooper()、UIスレッド)を引数に、
//        channelを取得します。返り値のchannelは、P2P機能を利用するのに必要なインスタンスです。
//        ここではinitializeメソッドの第3引数はnullですが、リスナーを登録でき、必要に応じてWiFi Direct接続が切れた場合(channelが失われた時)の通知を受け取れます。

        mWifiP2pManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        mChannel = mWifiP2pManager.initialize(this, getMainLooper(), null);
    }

    /** register the BroadcastReceiver with the intent values to be matched */
    @Override
    public void onResume() {
        super.onResume();
        // ブロードキャストレシーバで、WIFI_P2P_STATE_CHANGED_ACTIONのコールバックを持ってWi-Fi Direct ON/OFFを判定する
        mIsWiFiDirectEnabled = false;
        // たぶんこのタイミングでブロードキャストレシーバを登録するのがbetter
        registerBroadcastReceiver(ReceiverState.All);

        //        検索開始後リモートデバイスを検知した時の処理
        IntentFilter filter = new IntentFilter();
//        デバイス検索終了でブロードキャストされる
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
//        デバイスの検索を開始した時にブロードキャスト
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
//        デバイス検知の時点でブロードキャスト
        filter.addAction(BluetoothDevice.ACTION_FOUND);
//　　　　　デバイス名変更時にブロードキャスト
        filter.addAction(BluetoothDevice.ACTION_NAME_CHANGED);
        registerReceiver(DevieFoundReceiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        // ブロードキャストレシーバで、WIFI_P2P_STATE_CHANGED_ACTIONのコールバックを持ってWi-Fi Direct ON/OFFを判定する
        mIsWiFiDirectEnabled = false;
        // たぶんこのタイミングでブロードキャストレシーバを登録するのがbetter
        registerBroadcastReceiver(ReceiverState.All);
    }

    //NanoHTTPD起動
    class StartButtonClickListener01 implements View.OnClickListener {
        public void onClick(View v) {
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("NanoHttpd")
                    .setMessage("起動しました")
                    .setPositiveButton("OK", null)
                    .show();
            try {
                WebServer wb =new WebServer();
                wb.start();
            } catch (IOException e) {
            }
        }
    }

    //WifiDirect起動
    class StartButtonClickListener02 implements View.OnClickListener {
        public void onClick(View view) {
            mWifiP2pManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
            Log.w("TAG", "　Result[" + (mWifiP2pManager != null) + "]");
            onClickInitialize(view);
            mWifiP2pManager.requestConnectionInfo(mChannel, new WifiP2pManager.ConnectionInfoListener() {
                // requestConnectionInfo()実行後、非同期応答あり
                public void onConnectionInfoAvailable(WifiP2pInfo info) {
                    if (info == null) {
                        formatedIpAddress = "nullpoint";
                        return;
                    }
                    Log.w("TAG", "onClickRequestConnection");
                    Log.w("TAG", "info:" + info.groupOwnerAddress);
                    if (info.groupOwnerAddress != null) {
                        formatedIpAddress = info.groupOwnerAddress.toString();
                        TextView textIpaddr = (TextView) findViewById(R.id.ip);
                        textIpaddr.setText("Please access your browsers to http:/" + formatedIpAddress + ":" + 8080);
                    } else {
                        formatedIpAddress = null;
                    }
                }
            });
        }
    }

    //WifiDirect停止
    class StopButtonClickListener02 implements View.OnClickListener{
        public void onClick(View view){
            mWifiP2pManager.removeGroup(mChannel, mActionListenerAdapter);
        }
    }

    //Bluetooth検索許可
    class BluetoothFind implements View.OnClickListener{
        public void onClick(View view){
            //            Bluetoothペアリングの検索を可能にする
            Log.d("chk100", "捜索許可だすのんな");
            if (mybluetoothAdapter01.getScanMode() !=
                    BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
                Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 1000);
                startActivity(discoverableIntent);
            }

        }
    }

    //周辺のBluetooth検索
    //周辺のペアリングしていない端末を調べますの
    class BluetoothSearch implements View.OnClickListener {
        public void onClick(View v) {
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setTitle("Bluetooth情報");
            progressDialog.setMessage("取得中");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
            thread = new Thread(MainActivity.this);
            thread.start();
            Set<BluetoothDevice> pairedDevices = mybluetoothAdapter01.getBondedDevices();
            // Indicate scanning in the title
            // If we're already discovering, stop it
            if (mybluetoothAdapter01.isDiscovering()) {
                mybluetoothAdapter01.cancelDiscovery();
            }
            // Request discover from BluetoothAdapter
            mybluetoothAdapter01.startDiscovery();
        }
    }

    //    周辺のデバイス情報を検索する
    private final BroadcastReceiver DevieFoundReceiver = new BroadcastReceiver() {
        //検出されたデバイスからのブロードキャストを受ける
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            String dName = null;
            BluetoothDevice foundDevice;
            if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                Log.d("d", "スキャン開始");
            }
            // デバイス名変更の場合
            if (BluetoothDevice.ACTION_NAME_CHANGED.equals(action)) {
                // デバイスを表示用アダプターに設定映
                Log.d("d", "デバイス名変更");
            }
            // デバイス発見の場合
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // デバイスを表示用アダプターに設定
                Log.d("d", "デバイス発見");
                foundDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if ((dName = foundDevice.getName()) != null) {
                    if (foundDevice.getBondState() != BluetoothDevice.BOND_BONDED) {
                        //接続したことのないデバイスのみアダプタに詰める
                        Log.d("デバイス名", dName);
//                        Listに追加
                        btname01.add(dName);
//                        Bluetooth型にデバイス情報かくんのう
                        btinfor01.add(foundDevice);
                        Log.d("デバイス格納数", String.valueOf(btname01.size()));
                    }
                }
            }
            if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                Log.d("d", "スキャン終了");
                ArrayAdapter<String> adapter01 = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, btname01);
                ListView listView01 = (ListView) findViewById(R.id.btname01);
                listView01.setAdapter(adapter01);
                thread = new Thread(MainActivity.this);
                thread.start();
            }
        }
    };

    /**
     * リスナアダプタ
     * WifiP2pManagerクラスの各メソッドは、WifiP2pManager.ActionListenerによって、メソッドの実行結果を知ることができる
     * ただし、successと出たのに失敗したり、failureと出たのに成功したりする
     */
    class ActionListenerAdapter implements WifiP2pManager.ActionListener {
        // 成功
        public void onSuccess() {
        }
        // 失敗
        public void onFailure(int reason) {
        }

    }

    /**
     * P2Pメソッド実行前のNULLチェック
     */
    private boolean isNull(boolean both) {
        if (mActionListenerAdapter == null) {
            mActionListenerAdapter = new ActionListenerAdapter();
        }
        if (!mIsWiFiDirectEnabled) {
            return true;
        }

        if (mWifiP2pManager == null) {
            return true;
        }
        if (both && (mChannel == null) ) {
            return true;
        }

        return false;
    }

    public void onClickStopService(View view) {

    }

    /**
     * 初期化
     */
    public void onClickInitialize(View view) {
        if (isNull(false)) { return; }

        mChannel = mWifiP2pManager.initialize(this, getMainLooper(), new WifiP2pManager.ChannelListener() {
            public void onChannelDisconnected() {
            }
        });

        Log.w("TAG","　Result["+(mChannel != null)+"]");
        onClickRemoveGroup(view);
    }

    /**
     * グループ作成
     */
    public void onClickCreateGroup(View view) {
        if (isNull(true)) { return; }

        mWifiP2pManager.createGroup(mChannel, mActionListenerAdapter);
        Log.w("TAG", "onClickCreateGroup");

        onClickRequestConnectionInfo(view);
    }

    /**
     * グループ削除
     */
    public void onClickRemoveGroup(View view) {
        if (isNull(true)) { return; }

        mWifiP2pManager.removeGroup(mChannel, mActionListenerAdapter);

        //このタイミングでリストとかを全部nullすればwifiを消せるようになるかも
        Log.w("TAG", "onClickRemoveGroup");
        onClickCreateGroup(view);
    }

    /**
     * 接続情報要求
     */
    public void onClickRequestConnectionInfo(View view) {

        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        }

        if (isNull(true)) { return; }

        onClickRequestGroupInfo(view);
    }

    /**
     * グループ情報要求
     */
    public void onClickRequestGroupInfo(View view) {
        if (isNull(true)) { return; }

        mWifiP2pManager.requestGroupInfo(mChannel, new WifiP2pManager.GroupInfoListener() {
            // requestGroupInfo()実行後、非同期応答あり
            public void onGroupInfoAvailable(WifiP2pGroup group) {
                Log.w("TAG", "onClickGroup:" + group);
                if (group == null) {
                    return;
                }

                // パスワードは、G.O.のみ取得可能
                String pass = null;
                String ssid = null;
                if (group.isGroupOwner()) {
                    pass = group.getPassphrase();
                    ssid = group.getNetworkName();
                    TextView textSsid = (TextView) findViewById(R.id.ssid);
                    textSsid.setText("SSID: " + ssid);
                    TextView textPass = (TextView) findViewById(R.id.pass);
                    textPass.setText("Pass Word! " + pass);
                } else {
                    pass = "Client Couldn't Get Password";
                }
            }
        });

    }

    /**
     * ブロードキャストレシーバ 全部
     */
    public class WiFiDirectBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            String log = "onReceive() ["+action+"]";

            //WiFi Directの有効/無効
            if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
                mIsWiFiDirectEnabled = false;
                int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
                String sttStr;
                switch (state) {
                    case WifiP2pManager.WIFI_P2P_STATE_ENABLED:
                        mIsWiFiDirectEnabled = true;
                        sttStr = "ENABLED";
                        break;
                    case WifiP2pManager.WIFI_P2P_STATE_DISABLED:
                        sttStr = "DISABLED";
                        break;
                    default:
                        sttStr = "UNKNOWN";
                        break;
                }
                //changeBackgroundColor();
            } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
                // このタイミングでrequestPeers()を呼び出すと、peerの変化(ステータス変更とか)がわかる
                // 本テストアプリは、メソッド単位での実行をテストしたいので、ここではrequestPeers()を実行しない
            } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
                NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
                // networkInfo.toString()はCSV文字列(1行)を返す。そのままでは読みにくいので、カンマを改行へ変換する。
            } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
                WifiP2pDevice device = (WifiP2pDevice) intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_DEVICE);
            }
        }
    }

    private void registerBroadcastReceiver(ReceiverState rs) {
        IntentFilter filter = new IntentFilter();

        switch (rs) {
            case All:
                filter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
                filter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
                filter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
                filter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
                mReceiver = new WiFiDirectBroadcastReceiver();
                registerReceiver(mReceiver, filter);
                break;

            case StateChange:
                filter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
                mWDBR_P2P_STATE_CHANGED_ACTION = new WDBR_P2P_STATE_CHANGED_ACTION();
                registerReceiver(mWDBR_P2P_STATE_CHANGED_ACTION, filter);
                break;

            case PeersChange:
                filter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
                mWDBR_P2P_PEERS_CHANGED_ACTION = new WDBR_P2P_PEERS_CHANGED_ACTION();
                registerReceiver(mWDBR_P2P_PEERS_CHANGED_ACTION, filter);
                break;

            case ConnectionChange:
                filter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
                mWDBR_P2P_CONNECTION_CHANGED_ACTION = new WDBR_P2P_CONNECTION_CHANGED_ACTION();
                registerReceiver(mWDBR_P2P_CONNECTION_CHANGED_ACTION, filter);
                break;

            case ThisDeviceChange:
                filter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
                mWDBR_THIS_DEVICE_CHANGED_ACTION = new WDBR_P2P_THIS_DEVICE_CHANGED_ACTION();
                registerReceiver(mWDBR_THIS_DEVICE_CHANGED_ACTION, filter);
                break;

            default:
                break;
        }
    }

    /**
     * ブロードキャストレシーバ P2P ON/OFF状態変更検知
     */
    public class WDBR_P2P_STATE_CHANGED_ACTION extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            String log = "onReceive() ["+action+"]";

            if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
                mIsWiFiDirectEnabled = false;
                int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
                String sttStr;
                switch (state) {
                    case WifiP2pManager.WIFI_P2P_STATE_ENABLED:
                        mIsWiFiDirectEnabled = true;
                        sttStr = "ENABLED";
                        break;
                    case WifiP2pManager.WIFI_P2P_STATE_DISABLED:
                        sttStr = "DISABLED";
                        break;
                    default:
                        sttStr = "UNKNOWN";
                        break;
                }
                //changeBackgroundColor();
            }
        }
    }

    /**
     * ブロードキャストレシーバ 周辺のP2Pデバイス検出
     */
    public class WDBR_P2P_PEERS_CHANGED_ACTION extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            String log = "onReceive() ["+action+"]";

            if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
                // このタイミングでrequestPeers()を呼び出すと、peerの変化(ステータス変更とか)がわかる
                // 本テストアプリは、メソッド単位での実行をテストしたいので、ここではrequestPeers()を実行しない
            }
        }
    }

    /**
     * ブロードキャストレシーバ 接続状態変更(CONNECT/DISCONNECT)検知
     */
    public class WDBR_P2P_CONNECTION_CHANGED_ACTION extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            String log = "onReceive() ["+action+"]";

            if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
                NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
                // networkInfo.toString()はCSV文字列(1行)を返す。そのままでは読みにくいので、カンマを改行へ変換する。
            }
        }
    }

    /**
     * ブロードキャストレシーバ 自端末検知
     */
    public class WDBR_P2P_THIS_DEVICE_CHANGED_ACTION extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            String log = "onReceive() ["+action+"]";

            if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
                WifiP2pDevice device = (WifiP2pDevice) intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_DEVICE);
            }
        }
    }

    private class WebServer extends NanoHTTPD {
            public WebServer() throws IOException {
                super(8080);
            }
            @Override
            public Response serve(IHTTPSession session) {
                Map<String, String> parms = session.getParms();
                Map<String, String> parms2 = session.getParms();
                Log.d("NanoHttod", "起動したよ！");
                String msg ="<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
                        "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
                        "<head>\n" +
                        "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n" +
                        "    <title>Untitled Document</title>\n" +
                        "    <script type=\"text/javascript\">\n" +
                        "\n" +
                        "</script>\n" +
                        "\n" +
                        "</head>\n" +
                        "\n" +
                        "<html>\n" +
                        "<body>\n" +
                        "<h1>当ページの利用手順</h1><br>\n" +
                        "①\n" +
                        "<a href=\"data:,\n" +
                        "\n" +
                        "<!DOCTYPE html>%0A\n" +
                        "<html lang=&quot;ja&quot;>%0A\n" +
                        "%0A\n" +
                        "<head>%0A\n" +
                        "    <meta charset=&quot;utf-8&quot;>%0A\n" +
                        "    <title>TM Lib : Editor</title>%0A\n" +
                        "</head>%0A\n" +
                        "%0A\n" +
                        "<body>%0A\n" +
                        "<noscript>%0A\n" +
                        "    <p class=&quot;warning-message&quot; <a href=&quot;/web-app/support/javascript/&quot;>;JavaScriptを有効にする</a>と、サンプルを実行できます</p>%0A\n" +
                        "</noscript>%0A\n" +
                        "説明:txtarea内に好きな言葉を記述しダウンロードを行うとそのtxtファイルが記述されます%0A\n" +
                        "<div class=&quot;example-box&quot;>%0A\n" +
                        "    <textarea style=&quot;display: block&quot; rows=20 cols=100>県名,市名,必要なもの</textarea><a href=&quot;jtavascript:;&quot; download=&quot;sample.txt&quot;>ダウンロード</a>%0A\n" +
                        "</div>%0A\n" +
                        "<script type=&quot;text/javascript&quot;>%0A\n" +
                        "    (function() {%0A\n" +
                        "        var anchor = document.links[document.links.length - 1]%0A\n" +
                        "        if ('download' in anchor) {%0A\n" +
                        "            anchor.download = 'sample.txt';%0A\n" +
                        "            anchor.onclick = function() {%0A\n" +
                        "                var text = &quot;<ここから>--------------------------\\n&quot;%0A\n" +
                        "                text += this.previousSibling.value%0A\n" +
                        "                text += &quot;\\n-------------------------------<ここまで>&quot;%0A\n" +
                        "                this.href = 'data:,' + encodeURIComponent(text);v\n" +
                        "            }%0A\n" +
                        "        } else {%0A\n" +
                        "            var message = document.createElement('span');%0A\n" +
                        "            message.className = 'warning-message';%0A\n" +
                        "            message.appendChild(document.createTextNode('お使いのブラウザでは、download属性がサポートされていません。'));%0A\n" +
                        "%0A\n" +
                        "            anchor.parentNode.appendChild(message);%0A\n" +
                        "        }%0A\n" +
                        "    })();%0A\n" +
                        "    </script>%0A\n" +
                        "</br>%0A\n" +
                        "</br>%0A\n" +
                        "</br>%0A\n" +
                        "</br>%0A\n" +
                        "</br>%0A\n" +
                        "</br>%0A\n" +
                        "</br>%0A\n" +
                        "</br>ダウンロードしたファイルを選択してください</br>%0A\n" +
                        "<form name=&quot;test&quot;>%0A\n" +
                        "    <input type=&quot;file&quot; id=&quot;selfile&quot; valu>%0A\n" +
                        "    <br>%0A\n" +
                        "    <textarea name=&quot;txt&quot; rows=&quot;10&quot; cols=&quot;50&quot; readonly></textarea>%0A\n" +
                        "</form>%0A\n" +
                        "<script src=&quot;https://code.jquery.com/jquery-2.1.4.min.js&quot;></script>%0A\n" +
                        "<script src=&quot;https://cdn.jsdelivr.net/clipboard.js/1.5.3/clipboard.min.js&quot;></script>%0A\n" +
                        "<script>%0A\n" +
                        "    var obj1 = document.getElementById(&quot;selfile&quot;);%0A\n" +
                        "%0A\n" +
                        "    //ダイアログでファイルが選択された時%0A\n" +
                        "    obj1.addEventListener(&quot;change&quot;, function(evt) {%0A\n" +
                        "%0A\n" +
                        "        var file = evt.target.files;%0A\n" +
                        "%0A\n" +
                        "        //FileReaderの作成%0A\n" +
                        "        var reader = new FileReader();%0A\n" +
                        "        //テキスト形式で読み込む%0A\n" +
                        "        reader.readAsText(file[0]);%0A\n" +
                        "%0A\n" +
                        "        //読込終了後の処理%0A\n" +
                        "        reader.onload = function(ev) {%0A\n" +
                        "            //テキストエリアに表示する%0A\n" +
                        "            document.test.txt.value = reader.result;%0A\n" +
                        "        }%0A\n" +
                        "        var clipboard = new Clipboard('.btn');%0A\n" +
                        "    }, false);%0A\n" +
                        "%0A\n" +
                        "    $(function() {%0A\n" +
                        "        var clipboard = new Clipboard('.btn');%0A\n" +
                        "    });%0A\n" +
                        "    </script>%0A\n" +
                        "</body>%0A\n" +
                        "%0A\n" +
                        "</html>%0A\n" +
                        "\n" +
                        "\" download=\"localindex.html\">こちらを押してください(ローカルサイトがダウンロードされます)</a></br></br>\n" +
                        "\n" +
                        "②\n" +
                        "<!-- コピー対象 -->\n" +
                        "<input id=\"foo\" value=\"file:///sdcard/Download/localindex.html\" size=\"60\">\n" +
                        "（左のURLをコピーしてください）\n" +
                        "</br>今後は上記のURLで作業することになる\n" +
                        "<script>\n" +
                        "\n" +
                        "var cp = new Clipboard( \".btn\" ) ;\n" +
                        "\t</script>\n" +
                        "\n" +
                        "\n" +
                        "</br></br>③下の状態ボックスに「切断」という文字列を入力してください(オーナーとの接続がきれます)</br></br>\n" +
                        "④③以降の動作は②でコピーしたローカルサイトに操作するので今観覧しているブラウザで</br>file:///sdcard/Download/localindex.htmlにアクセスしてください</br>\n" +
                        "<h1>③：状態ボックス</h1>\n";
                if (parms.get("username") == null) {
                    msg += "<form action='?' method='get'>\n  <p>状態ボックス(切断を入力してください) <input type='text' name='username'></p>\n" + "</form>\n";
                } else if(parms.get("username").equals("切断")){
                    mWifiP2pManager.removeGroup(mChannel, mActionListenerAdapter);
                    TextView textSsid = (TextView) findViewById(R.id.ssid);
                    textSsid.setText("SSID: Null");
                    TextView textPass = (TextView) findViewById(R.id.pass);
                    textPass.setText("Pass Word! NuLL");
                    mWifiP2pManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
                    mChannel = mWifiP2pManager.initialize(MainActivity.this, getMainLooper(), null);
                }
                else {
                    msg += "<form action='?' method='get'>\n  <p>入力が誤っています: <input type='text' name='username'></p>\n" + "</form>\n";
                }

                return newFixedLengthResponse(msg + "</body></html>\n");
            }
        }

    @Override
    public void run(){
        progressDialog.dismiss();
    }
}
